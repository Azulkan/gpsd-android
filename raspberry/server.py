#!/usr/bin/env python3
# coding: utf-8

import gpsd
import json
import socket

def getGpsInfo():
	packet = gpsd.get_current()
	data = {
        	'mode': str(packet.mode),
        	'latitude': str(packet.lat),
        	'longitude': str(packet.lon),
        	'time': str(packet.time),
        	'altitude': str(packet.altitude),
        	'device': str(gpsd.device)
    	}
	return str(data)

if __name__ == "__main__":
	# Connect to the local gpsd
	gpsd.connect(host='127.0.0.1')

	host = ''
	port = 9999

	connexion = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
	connexion.bind((host, port))

	while True:
		print('The server is listening the port {}.'.format(port))
		connexion.listen(5)

		client_connexion, infos_connexion = connexion.accept()
		print('Connexion from: {}.'.format(infos_connexion))

		print('Sending GPS infos to the client')
		client_connexion.send(getGpsInfo().encode())

		print('Connexion closing.')
		client_connexion.close()
	connexion.close()
