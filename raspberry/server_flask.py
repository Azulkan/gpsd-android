#!/usr/bin/env python3
# coding: utf-8
import gpsd
import json
from flask import Flask, jsonify, request, render_template

# Connect to the local gpsd
gpsd.connect(host='127.0.0.1')

# Get gps position
packet = gpsd.get_current()

# Configuration of the Flask server
app = Flask("__name__")

# Index
@app.route("/")
def kikou():
    return "You are at the root of the flask server hosted on the Raspberry Pi.\n You can acces GPS data at /gps."

@app.route("/gps", methods =["GET"])
def getGpsInfo():
    packet = gpsd.get_current()
    data = {
        'mode': str(packet.mode),
        'latitude': str(packet.lat),
        'longitude': str(packet.lon),
        'time': str(packet.time),
        'altitude': str(packet.altitude),
        'device': str(gpsd.device)
    }
    return jsonify(data)

if __name__ == "__main__":
    app.run(host="0.0.0.0", port=8080, threaded=True)
