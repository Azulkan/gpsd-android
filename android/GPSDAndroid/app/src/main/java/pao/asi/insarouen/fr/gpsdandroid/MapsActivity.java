package pao.asi.insarouen.fr.gpsdandroid;

import android.Manifest;
import android.content.Context;
import android.location.LocationManager;
import android.location.LocationProvider;
import android.location.LocationListener;
import android.location.Location;

import android.os.Handler;
import android.os.SystemClock;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import org.json.JSONException;
import org.json.JSONObject;

public class MapsActivity extends FragmentActivity implements OnMapReadyCallback, LocationListener, Client.AsyncResponse {
    private GoogleMap mMap;
    private Marker marker;
    private LocationManager mLocationManager;
    private static final String PROVIDER = LocationManager.GPS_PROVIDER;
    private static final int REFRESH_TIME = 2000; // In milliseconds
    private EditText portField;
    private EditText addressField;
    private ImageView connection;
    private Context context;
    private String address;
    private int port; // Those of the Raspberry Pi
    private boolean newUrl = false;

    /* This is used to continously retrieve data from the server */
    private Handler myHandler;
    private Runnable myRunnable = new Runnable() {
        @Override
        public void run() {
            /* What to do */
            Client myClient = new Client((Client.AsyncResponse) context, address, port);
            myClient.execute();

            /* Do all this again, after refresh time */
            myHandler.postDelayed(this, REFRESH_TIME);
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);
        context = this;

        addressField = (EditText) findViewById(R.id.EditTextAddress);
        portField = (EditText) findViewById(R.id.EditTextPort);
        connection = (ImageView) findViewById(R.id.connection);
        connection.setTag(R.mipmap.ic_off);

        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, 1);

        /* Create the LocationManager and set the Provider to GPS_PROVIDER because this is where the location will be mocked */
        mLocationManager = (LocationManager) getSystemService(LOCATION_SERVICE);
        mLocationManager.requestLocationUpdates(PROVIDER, 50, 0, this);
    }

    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        LatLng coords = new LatLng(0, 0);
        BitmapDescriptor icon = BitmapDescriptorFactory.fromResource(R.mipmap.ic_marker);
        marker = mMap.addMarker(new MarkerOptions().position(coords).title("Position of the raspberry").icon(icon));
        marker.setVisible(false);
    }

    /**
     * If needed, changes the provider of mLocationManager to GPS_PROVIDER.
     * Sets the mock location to mLocationManager
     */
    private void getMockLocation(double lat, double lon) {
        if(mLocationManager.isProviderEnabled(PROVIDER))
            //mLocationManager.removeTestProvider(LocationManager.GPS_PROVIDER);

        mLocationManager.addTestProvider
        (
            PROVIDER,
            "requiresNetwork" == "",
            "requiresSatellite" == "",
            "requiresCell" == "",
            "hasMonetaryCost" == "",
            "supportsAltitude" == "",
            "supportsSpeed" == "",
            "supportsBearing" == "",

            android.location.Criteria.POWER_LOW,
            android.location.Criteria.ACCURACY_FINE
        );

        Location newLocation = new Location(PROVIDER);

        newLocation.setLatitude(lat); // Get these value from the raspberry
        newLocation.setLongitude(lon);
        newLocation.setTime(System.currentTimeMillis());
        newLocation.setElapsedRealtimeNanos(SystemClock.elapsedRealtimeNanos());
        newLocation.setAccuracy(500);

        mLocationManager.setTestProviderEnabled
        (
                PROVIDER,
                true
        );

        mLocationManager.setTestProviderStatus
        (
                PROVIDER,
                LocationProvider.AVAILABLE,
                null,
                System.currentTimeMillis()
        );

        mLocationManager.setTestProviderLocation
        (
                PROVIDER,
                newLocation
        );

        System.out.println("The location has been mocked to the provider.");
    }

    @Override
    public void onProviderDisabled(String provider) {

    }

    @Override
    public void onProviderEnabled(String provider) {

    }

    @Override
    public void onLocationChanged(Location loc) {
        System.out.println("Location changed !");
        LatLng coords = new LatLng(loc.getLatitude(), loc.getLongitude());
        BitmapDescriptor icon = BitmapDescriptorFactory.fromResource(R.mipmap.ic_marker);
        marker.remove();
        marker = mMap.addMarker(new MarkerOptions().position(coords).title("Position of the raspberry").icon(icon));
        mMap.moveCamera(CameraUpdateFactory.newLatLng(coords));
    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {

    }

    public void updateURL(View v) {
        /* Update Raspberry's address and port */
        address = addressField.getText().toString();
        port = Integer.valueOf(portField.getText().toString());
        newUrl = true;

        /* As the runnable calls itself, what follows is basically a loop with a delay */
        myHandler = new Handler();
        myHandler.postDelayed(myRunnable, REFRESH_TIME);

        /* Add a listener to switch the connexion off */
        connection.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(myHandler != null) {
                    myHandler.removeCallbacks(myRunnable);
                }
                if((Integer) connection.getTag() == R.mipmap.ic_on) {
                    connection.setImageResource(R.mipmap.ic_off);
                    connection.setTag(R.mipmap.ic_off);
                    if(marker.isVisible()) marker.setVisible(false);
                }
                Toast.makeText(context, "Stopped requesting the Raspberry Pi's position.", Toast.LENGTH_LONG).show();
            }
        });
    }

    /* This method from AsyncResponse interface allows to process the response from the server in this activity */
    @Override
    public void processFinish(String response) {
        try {
            /* Mock the location ! */
            System.out.println("Mocking location...");
            JSONObject json = new JSONObject(response);
            getMockLocation(json.getDouble("latitude"), json.getDouble("longitude"));

            /* Show a healthy connexion with the raspberry if it is not the case */
            if((Integer) connection.getTag() == R.mipmap.ic_off) {
                connection.setImageResource(R.mipmap.ic_on);
                connection.setTag(R.mipmap.ic_on);
                if(!marker.isVisible()) marker.setVisible(true);
            }

            /* If this is the first time since URL update, notify success */
            if(newUrl) {
                Toast.makeText(context, "Connexion established with the Raspberry Pi. Requesting position every " + String.valueOf(REFRESH_TIME) + " ms.", Toast.LENGTH_LONG).show();
                newUrl = false;
            }
        } catch(JSONException e) {
            /* Show an unhealthy connexion with the Raspberry */
            if((Integer) connection.getTag() == R.mipmap.ic_on) {
                connection.setImageResource(R.mipmap.ic_off);
                connection.setTag(R.mipmap.ic_off);
                if(marker.isVisible()) marker.setVisible(false);
            }
            Toast.makeText(context, "Could not retrieve correct data from the Raspberry Pi. Check address and port.", Toast.LENGTH_LONG).show();
            e.printStackTrace();
        }
    }
}
