package pao.asi.insarouen.fr.gpsdandroid;

import android.os.AsyncTask;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.Socket;
import java.net.UnknownHostException;

/**
 * Created by pauls on 18/12/17.
 */

public class Client extends AsyncTask<Void, Void, String> {
    private String dstAddress;
    private int dstPort;
    private String response = "";

    public interface AsyncResponse {
        void processFinish(String output);
    }

    private AsyncResponse delegate = null;

    Client(AsyncResponse delegate, String addr, int port) {
        this.delegate = delegate;
        dstAddress = addr;
        dstPort = port;
    }

    @Override
    protected String doInBackground(Void... arg0) {
        Socket socket;

        try {
            socket = new Socket(dstAddress, dstPort);
            ByteArrayOutputStream outStream = new ByteArrayOutputStream(1024);
            byte[] buffer = new byte[1024];
            int bytesRead;
            InputStream inStream = socket.getInputStream();

            bytesRead = inStream.read(buffer);
            outStream.write(buffer, 0, bytesRead);
            response += outStream.toString("UTF-8");
            System.out.println(response);
            return(response);
        } catch(UnknownHostException e) {
            e.printStackTrace();
            response = "Unknown host exception: " + e.toString();
        } catch(IOException e) {
            e.printStackTrace();
            response = "IO exception: " + e.toString();
        }
        return "";
    }

    /* When doInBackground is finished, this methods delegates the processing of the returned value to the activity */
    @Override
    protected void onPostExecute(String response) {
        delegate.processFinish(response);
    }
}
